#ifndef LAB1_IMG_ROTATOR_H
#define LAB1_IMG_ROTATOR_H

#include "bmp.h"

struct image image_rotate(struct image const source );


#endif //LAB1_IMG_ROTATOR_H
