#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>


#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType; //Сигнатура bmp - "BM", равняется 19778
    uint32_t  bfileSize; //Размер файла
    uint32_t bfReserved;
    uint32_t bOffBits; //Расстояние между началом файла и пикселями, считается за размер заголовка файла
    uint32_t biSize; //Размер так называемого InfoHeader'а, константа
    uint32_t biWidth; //Ясно
    uint32_t  biHeight; //Ясно
    uint16_t  biPlanes; //Константа
    uint16_t biBitCount; //Сколько битов используется на один пиксель
    //Этим можно пренебречь
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)
//Как в старом добром array_int_read(){})
static struct bmp_header bmp_header_read(FILE* f) {
    struct bmp_header header = {0};
    fread( &header, sizeof( struct bmp_header ), 1, f );
    return header;
}

static struct bmp_header bmp_header_create(const struct image* img){
    const struct bmp_header header = (struct bmp_header){
            .bfType = 0x4D42, //19778 in hex
            .bfileSize = img->width * img->height * sizeof(struct pixel) + img->height * (img->width % 4) + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
//    const struct bmp_header* ptr = &header;
    return header;
}

static uint8_t get_padding(const struct  image* img){
    uint8_t padding = 4 - img->width * 3 % 4;
    if(padding == 4) padding = 0;
    return padding;
}

static enum read_status bmp_header_is_correct(struct bmp_header header){
    if (header.bfType != 19778) return READ_INVALID_SIGNATURE;
    if (header.biBitCount != 24) return READ_INVALID_BITS;
    return READ_OK;
}

enum read_status from_bmp( FILE* input, struct image* img){
    struct bmp_header header = bmp_header_read(input);
    const enum read_status hd_corr = bmp_header_is_correct(header);
    if(hd_corr != READ_OK) return hd_corr;
    img->data = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
    img->height = header.biHeight;
    img->width = header.biWidth;
    const uint8_t padding = get_padding(img);

    for (uint32_t row = 0; row < header.biHeight; row++) {
        if (fread(row * header.biWidth + img->data, sizeof(struct pixel), header.biWidth, input) == 0)
            return READ_END_OF_FILE;
        fseek(input, padding, SEEK_CUR);
    }
    return READ_OK;

}


enum write_status to_bmp( FILE* out, struct image const* img ){
    const struct bmp_header temp_header = bmp_header_create(img);
    fwrite(&temp_header, sizeof (struct bmp_header), 1, out);
    const uint8_t padding = get_padding(img);
    for(size_t i = 0; i < img->height; i++) {
        fwrite(img->data+i*img->width, sizeof(struct pixel), img->width, out);
        const int8_t pad = 0;
        if(padding != 0) fwrite(&pad, sizeof (uint8_t), padding, out);
    }
    return WRITE_OK;
}
