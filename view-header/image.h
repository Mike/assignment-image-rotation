#ifndef LAB1_IMAGE_H
#define LAB1_IMAGE_H


#include <stdint.h>
#include <malloc.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void image_free(struct image* im);

#endif //LAB1_IMAGE_H
