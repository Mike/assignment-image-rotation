#include <stdbool.h>
#include <stdio.h>

#include "bmp.h"
#include "img_rotator.h"

bool rotate_image(const char* source, const char* destination){
    struct image pic, second;
    bool success = true;
    FILE* input = fopen(source, "rb");
    FILE* output = fopen(destination, "wb");
    success = !from_bmp(input, &pic);
    if(!success){
        fprintf(stderr, "Failed to read file");
        return false;
    }
    second = image_rotate(pic);
    success &= !to_bmp(output, &second);
    if(!success){
        fprintf(stderr, "Failed to write to file");
        return false;
    }
    fclose(input);
    fclose(output);
    image_free(&pic);
    image_free(&second);
    return success;

}

int main() {
    printf("Enter source and destination file names...\n");
    char s[64], d[64];
    scanf("%s", s);
    scanf("%s", d);
    if(!rotate_image(s, d)) fprintf(stdout, "Rotation was not preformed");
    else fprintf(stdout, "Image was rotated");

    return 0;
}
