#include "img_rotator.h"
#include <malloc.h>
#include <stdint.h>

struct image image_rotate(struct image const source ){
    const struct image img = (struct image){
            .height = source.height,
            .width = source.width,
            .data = malloc(source.width * source.height * sizeof (struct pixel))
    };

    for (uint64_t i = 0; i < source.width; ++i) {
        for (uint64_t j = 0; j < source.height; ++j) {
            img.data[i * img.width + img.width - j - 1] = source.data[j * source.width + i];
        }
    }

    return img;

}
